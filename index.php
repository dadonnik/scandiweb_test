<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 17.04.2018
 * Time: 13:43
 */

	// During development show all errors
	ini_set('display_errors', 1);
	error_reporting(E_ALL);


	define('__URL__', $_SERVER["HTTP_HOST"]."/" );

	define('__ROOT__', dirname(__FILE__));

	// Include basic classes to start class autoloading with custom exception handling
	require_once "components/InvalidRouteException.php";
	require_once  "components/SPLAutoloader.php";

	///////////////////////////////////////////////////////////////////////
	// Initialize router class
	use components\Router;

	// In class TSPLAutoloader run method autoload
	spl_autoload_register( array('TSPLAutoloader', 'autoload') );

	// Catch exception
	set_exception_handler('handleException');

	function handleException (Exception $e)
	{

		if($e instanceof InvalidRouteException) {
			echo "Page404";
		}else{
			echo "Error";
		}

	}
	$router = new Router();
	$router->run();



?>