<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 20.04.2018
 * Time: 18:37
 */

?>

<body>
<div class="container">
	<header>
		<div class="row">
			<div class="col-md-4">
				<div class="page-title">
					<h2>Product Add</h2>
				</div>
			</div>
			<div class="col-md-4 offset-md-4">
				<input type="submit" name="add_new" value="Save" class="add_new_btn">
			</div>
		</div>

	</header>
    <div class="content">
        <?php echo $data ?>
        <form action="" method="post" class="add_new">
            <div class="form_field">
                <label>SKU</label>
                <input type="text" name="SKU" class="input_sku" required>
            </div>

            <div class="form_field">
                <label>Name</label>
                <input type="text" name="Name" class="input_name" required>
            </div>

            <div class="form_field">
                <label>Price</label>
                <input type="text" name="Price" class="input_price" required>
            </div>

            <div class="form_field">
                <label>Type Switcher</label>
                <select name="t_s" class="input_type_switcher" required>
                    <option value="">Type Switcher</option>
                    <option value="Furniture">Furniture</option>
                    <option value="Book">Book</option>
                    <option value="DVD">DVD-disc</option>
                </select>
            </div>

        </form>
    </div>
