<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 18.04.2018
 * Time: 20:32
 */
	?>
<body>
<div class="container">
    <header>
        <div class="row">
            <div class="col-md-4">
                <div class="page-title">
                    <h2>Product list</h2>
                </div>
            </div>
            <div class="col-md-4 offset-md-4">
                <form action="#" method="post" class="mass_actions">
                    <select name="mass_action">
                        <option value="mass_delete">Delete selected</option>
                    </select>
                    <input type="submit" name="submit" value="Apply">
                </form>
            </div>
        </div>

    </header>




	<div class="content">
	<div class="row">
		<?php foreach ($data as $key=>$value):?>
				<div class="col-md-3 product">
					<input type="checkbox" class="product_selected">
					<div class="product_info">
						<ul>
						<?php foreach ($value as $k=>$v):?>
							<li class="field" name="<?php echo $k ?>"><?php echo $v ?></li>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>
			<?php endforeach; ?>

	</div>
</div>


