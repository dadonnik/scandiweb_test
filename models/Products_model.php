<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 18.04.2018
 * Time: 21:01
 */
namespace models;
use components\DB;
use components\TQuery;
use \PDO;

class Products_model {

	// Load data
	public static function getProductList()
	{
		$products = array();
		$db = new DB();
		$query = TQuery::sql()->select("*")
		               ->from("Products" )
		               ->get();
		$result = $db->execute($query);

		foreach ($result as $key=>$value){
			$t = & $result[$key]["Type"];
			$e = & $result[$key];
			$e = $value;
			if ($t === "Book")
				$e["Special"].=" KG";
			//else if ( $t === "Furniture")
			//	$e["Special"].=" mm";
			else if ($t === "Disc")
				$e["Special"].=" MB";

			$products[$key] = $e;
		}

		return $products;

	}

	public static function addNew($params){
		$SKU = $params["SKU"];
		$Name = $params["Name"];
		$Price = $params["Price"];
		$Type = $params["t_s"];
		$Special = $params["Special"];

		$db = new DB();
		$query = "INSERT INTO Products(SKU, Name, Price,Type, Special) VALUES('$SKU','$Name','$Price','$Type','$Special')";
		$result = $db->execute($query);

		return $result;

	}

	// Delete selected products
	public static function deleteProductList($whatDelete){
		$db = new DB();
		$where = "(";
		// Concatenate where string
		foreach ($whatDelete as $key=>$value){
			$where = $where . "SKU='".$value->SKU."' OR ";
		}

		// Delete last OR
		$where = substr($where, 0, strlen($where)-3 );
		$where .= ')';


		$query = TQuery::sql()->delete()
			->from("Products")
			->where($where)
			->get();

		$result = $db->execute($query);
	}



}