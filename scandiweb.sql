-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 25 2018 г., 16:24
-- Версия сервера: 5.5.53
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `scandiweb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `SKU` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Price` float NOT NULL,
  `Type` varchar(255) NOT NULL,
  `Special` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`SKU`, `Name`, `Price`, `Type`, `Special`) VALUES
('GGWP0009', 'War and Peace', 20, 'Book', '2'),
('GGWP0011', 'War and Peace', 20, 'Book', '2'),
('GGWP0012', 'War and Peace', 20, 'Book', '2'),
('GGWP0013', 'War and Peace', 20, 'Book', '2'),
('GGWP0014', 'War and Peace', 20, 'Book', '2'),
('GGWP1111', 'War and Peace', 123, 'Book', '1'),
('JVC200123', 'Acme DISC', 100, 'Disc', '700'),
('JVC200124', 'Acme DISC', 100, 'Disc', '700'),
('JVC200125', 'Acme DISC', 100, 'Disc', '700'),
('JVC200126', 'Acme DISC', 100, 'Disc', '700'),
('JVC200127', 'Acme DISC', 100, 'Disc', '700'),
('JVC200128', 'Acme DISC', 100, 'Disc', '700'),
('JVC200129', 'Acme DISC', 100, 'Disc', '700'),
('JVC200130', 'Acme DISC', 100, 'Disc', '700'),
('TR120555', 'Chair', 40, 'Furniture', '24x45x15'),
('TR120556', 'Chair', 40, 'Furniture', '24x45x15'),
('TR120557', 'Chair', 40, 'Furniture', '24x45x15'),
('TR120558', 'Chair', 40, 'Furniture', '24x45x15'),
('TR120559', 'Chair', 40, 'Furniture', '24x45x15'),
('TR120560', 'Chair', 40, 'Furniture', '24x45x15'),
('TR120562', 'Chair', 40, 'Furniture', '24x45x15');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`SKU`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
