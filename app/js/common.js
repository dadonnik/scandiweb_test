$(function() {


	var showSpecialField = function(options){
	 
	    /*
	     * Variables accessible
	     * in the class
	     */	
	    var vars = {
	        knownTypes  : ["Book", "DVD", "Furniture"],
	        currentType : "unknown"
	    };
	 
	    /*
	     * Can access this.method
	     * inside other methods using
	     * root.method()
	     */
	    var root = this;
	 
	    /*
	     * Constructor
	     */
	    this.construct = function(options){
	        $.extend(vars , options);
	        if ( $.inArray(vars.currentType, vars.knownTypes) == -1)
	        	vars.currentType = "unknown";
	    };



	     /*
	     * Public method
	     * Can be called outside class
	     */

	    this.create = function(){
	    	if( $(".input_special").length !== 0 )
	    		$(".input_special").parent().remove();
	    	return getTemplate(getDescByType(vars.currentType), getLabelByType(vars.currentType));
	    }
	 
	     /*
	     * Private methods
	     * Can only be called inside class
	     */
	    var getDescByType = function(type){
			switch(type){
				case "Book":
					return "Please input book weight in KG";

				case "DVD":
					return "Please input disc size in MB";	

				case "Furniture":
					return "Please input size as AxBxC";

				default:
					return "Desc unknown";		
			}    	
	    };

	    var getLabelByType = function(type){
			switch(type){
				case "Book":
					return "Weight";

				case "DVD":
					return "Size";	

				case "Furniture":
					return "Size";

				default:
					return "Label unknown";		
			}    	
	    };    
	 
	    var getTemplate = function(desc, label){
	    	text = "<div class='form_field'>"+
	    	"<p>"+desc+"</p>"+
	    	"<label>"+label+"</label>"+
	    	"<input type='text' name='Special' class='input_special'>";
	    	return $.parseHTML(text);
	    };
	 
	 
	    /*
	     * Pass options when class instantiated
	     */
	    this.construct(options);
	 
	};


	var formValidate = function(options){
	 
	    /*
	     * Variables accessible
	     * in the class
	     */	
	    var vars = {
	        knownTypes  : ["Book", "DVD", "Furniture"],
	        formClass : ".add_new"
	    };
	 
	    /*
	     * Can access this.method
	     * inside other methods using
	     * root.method()
	     */
	    var root = this;
	 
	    /*
	     * Constructor
	     */
	    this.construct = function(options){
	        $.extend(vars , options);
	    };

	    this.validate = function(){
	    	SKU = $(vars.formClass).find(".input_sku").val();
	    	Name = $(vars.formClass).find(".input_name").val();	   
	    	Price = $(vars.formClass).find(".input_price").val(); 	
	    	Type  = $(vars.formClass).find(".input_type_switcher option:selected").val();
	    	Special = $(vars.formClass).find(".input_special").val();

	    	isOk = true;
	    	if (!validateSKU(SKU)){
	    		isOk = false;
	    		alert("Input error in SKU, please provide name, 8-12 characters length, use only letters and numbers");
	    	}
	    	if (!validatePrice(Price)){
	    		isOk = false;
	    		alert("Input error in Price, please provide only integer or float numbers");	   
	    	}
	    	if (!validateSpecial(Type,Special)){
	    		isOk = false;
	    		alert("Input error in Additional, please select type and provide correct data");	   
	    	}

	    	return isOk;
	    }
	 
	    /*
	     * Public method
	     * Can be called outside class
	     */
	    var validateSKU = function(SKU){
 			return /^([0-9a-zA-Z]{8,12})$/.test(SKU);
	    };

	    var validatePrice = function(Price){
 			return /^[+-]?([0-9]*[.])?[0-9]+$/.test(Price);
	    };	    

	    var validateSpecial = function(type, input){
	    	if ( $.inArray(type,vars.knownTypes) !== -1 ){
	    		switch(type){
					case "Furniture":
						return /^\d+x\d+x\d+$/.test(input);
					default:
						return validatePrice(input);    			
	    		}
	    	}
	    };

  
	
	 
	    /*
	     * Pass options when class instantiated
	     */
	    this.construct(options);
	 
	};





	$(".mass_actions").on("submit",function(e){
	//	e.preventDefault();
		data = [];
		$(".product_selected:checked").each(function(){
			
			fields = $(this).parent().find(".field");
			var element = {};
			$(fields).each(function(){
				name = $(this).attr("name");
				val = $(this).text();
				element[name] = val;
			});


			data.push(element);
		})
        var input = $("<input>").attr("type", "hidden").attr("name", "data").val(JSON.stringify(data));
        $(this).append($(input));
		return true;

	})


	$(".input_type_switcher").change(function(){
		type = $(this).find("option:selected").val();
		spec = new showSpecialField({"currentType":type});
		obje = spec.create();
		$(".add_new").append(obje);
	})


	$(".add_new_btn").click(function(e){
		e.preventDefault();
		validator = new formValidate();
		if ( validator.validate() ){
			$(".add_new").submit();
		}
	})


});
