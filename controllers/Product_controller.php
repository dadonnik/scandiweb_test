<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 17.04.2018
 * Time: 20:51
 */

namespace controllers;
use models\Products_model;

class Product_controller extends Controller {


	public function actionProductList()
	{
		// Delete event
		if(isset($_POST["submit"])){
			$whatDelete = json_decode($_POST["data"]);
			Products_model::deleteProductList($whatDelete);
		}
		// Load data
		$data = Products_model::getProductList();

		// Show page
		return $this->render('products/viewPage',$data);
	}


	public function actionAdd()
	{
		if(!empty($_POST)){
			$data = Products_model::addNew($_POST);
			$message = "Unknown";
			if ($data !== false ){
				$message = "Succesfully added";
			}
			else{
				$message =  "Something was wrong";
			}
		}
		return $this->render('products/viewAdd',$message);
	}

}