<?php
namespace controllers;
class Controller
{
	// Template file
	public $layoutFile = 'views/Layout.php';

	public function renderLayout ($body)
	{

		ob_start();
		require __ROOT__."/template/Layout.php";
		return ob_get_clean();

	}

	public function render ($viewName,  $data)
	{
		// Load view file
		$viewFile = __ROOT__.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.$viewName.'.php';
		// Start output buffering
		ob_start();
		require $viewFile;
		// Get buffer and clear it
		$body = ob_get_clean();
		return $this->renderLayout($body);

	}

}