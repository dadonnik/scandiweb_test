<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 18.04.2018
 * Time: 20:58
 */

namespace components;

class DB {
	public $pdo;

	// Create PDO connection
	public function __construct()
	{
		$paramsPath = __ROOT__ . '/config/db_params.php';
		$params = include($paramsPath);
		$dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";

		$this->pdo = new \PDO($dsn, $params['user'], $params['password']);
	}

	public function execute($query, array $params=null)
	{
		$stmt = $this->pdo->prepare($query);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		if ($stmt->execute($params)) {
			if ( $stmt->rowCount() > 0 ) {
				$test = $stmt;
				return $stmt->fetchAll();
			}
			else if ($stmt->errorCode() === "00000" )
				return true;
			else
				return false;
		}
		else
			return false;

	}


}