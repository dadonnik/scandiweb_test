<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 17.04.2018
 * Time: 14:03
 */


namespace components;
class Router {

	private $routes;

	public function __construct() {
		// load file with available routes
		$routesPath   = __ROOT__ . '/config/routes.php';
		$this->routes = include( $routesPath );
	}

// Return type

	private function getURI() {
		if ( ! empty( $_SERVER['REQUEST_URI'] ) ) {
			return trim( $_SERVER['REQUEST_URI'], '/' );
		}
	}

	public function run() {
		$uri = $this->getURI();
		$notFound = true;

		// Check all patterns
		foreach ( $this->routes as $uriPattern => $path ) {

			if ( preg_match( "~$uriPattern~", $uri ) ) {
				$notFound = false;

				// Get internal route
				$internalRoute = preg_replace( "~$uriPattern~", $path, $uri );

				// controllerName/actionName/params
				$segments = explode( '/', $internalRoute );

				$controllerName = ucfirst(array_shift( $segments )) . '_controller';
				$actionName = 'action'. ucfirst(array_shift( $segments ));
				$parameters = $segments;

				// Include controller file
				$controllerFile = __ROOT__ . '/controllers/' .$controllerName. '.php';
				if (!file_exists($controllerFile)) {
					throw new \InvalidRouteException();
				}

				$controllerName = "controllers\\$controllerName";

				$controllerObject = new $controllerName;

				if (!class_exists($controllerName)){
					throw new \InvalidRouteException();
				}

				// Run controller->action(params)
				$result = @call_user_func_array( array( $controllerObject, $actionName ), $parameters );
                echo $result;
				if ( $result != null ) {
					break;
				}
				else{
					throw new \InvalidRouteException();
				}

			}
		}
		if ($notFound)
			throw new \InvalidRouteException();
	}
}

