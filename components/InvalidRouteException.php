<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 17.04.2018
 * Time: 14:13
 */
class InvalidRouteException extends Exception
{
	public function __construct($message="", $code = 0, Exception $previous = null) {
		parent::__construct($message, $code, $previous);
	}

	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}
}