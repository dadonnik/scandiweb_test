<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 17.04.2018
 * Time: 14:19
 */


require_once "InvalidRouteException.php";

class TSPLAutoloader {

	public static function autoload($className) {
		$name = str_replace('\\','/',$className.'.php');
		if(file_exists($name)){
			require_once($name);
			return true;
		}
		self::throwFileNotFoundException($name);
	}

	public static function throwFileNotFoundException($name)
	{
		throw new InvalidRouteException();
	}
}