<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 08.03.2018
 * Time: 13:29
 */

namespace components;
class TQuery {
	private static $instance = NULL;

	public static function sql(){
		self::$instance = new self();
		return self::$instance;
	}

	// These methods set the query clauses
	// Use * as default
	public function select($select_options = "*") {

		$query_part = "SELECT ".$select_options;
		$this->select = $query_part;
		return $this;
	}

	public function delete($delete_options = "") {

		$query_part = "DELETE ".$delete_options;
		$this->select = $query_part;
		return $this;
	}


	// From table in DB
	public function from($from_options) {

		$query_part = "FROM ".$from_options;

		$this->from = $query_part;
		return $this;
	}

	public function limit($limit_options) {

		$query_part = "LIMIT ".$limit_options;

		$this->limit = $query_part;
		return $this;
	}




	// These methods set the query clauses
	public function where($where_options) {

		$query_part = "WHERE ".$where_options;
		$this->where = $query_part;
		return $this;
	}

	// GroupBy
	public function groupBy($group_options) {

		$query_part = "groupBy ".$group_options;
		$this->groupBy = $query_part;
		return $this;
	}



	public function get() {
		$array = (array)$this;
		$string = implode(" ", $array);

		return $string;
	}
}